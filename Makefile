SHELL = /bin/bash

setup:;
	@echo "Generating secret files from templates using environment file + variables"
	@source .env && rm -f ./config/application/secret.properties && envsubst < config/application/secret.properties.sample > config/application/secret.properties

dev-start:;
	mvn compile -e quarkus:dev -Dconfig.secret.path=$$PWD/config/application/secret.properties

clean:;
	mvn clean
	yarn run clean

install-yarn:;
	yarn install --frozen-lockfile --audit

compile-test-resources: install-yarn;
	yarn run generate-json-schema

compile-java: compile-test-resources;
	mvn compile package

compile-java-quick: compile-test-resources;
	mvn compile package -Dmaven.test.skip=true

compile: clean compile-java;

compile-quick: clean compile-java-quick;

compile-start: compile-quick;
	docker compose down
	docker compose build
	docker compose up -d

start-spec: compile-test-resources;
	yarn run start