/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.precaches;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.ProcessingException;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.LoadingCacheManager.LoadingCacheProvider;
import org.eclipsefoundation.media.api.YoutubeAPI;
import org.eclipsefoundation.media.api.models.YoutubePlaylistResponse;
import org.eclipsefoundation.media.api.models.YoutubeRequestParams;
import org.eclipsefoundation.media.config.YoutubeMediaProviderConfig;
import org.eclipsefoundation.media.models.YoutubePlaylist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Precache provider for YoutubePlaylist entities.
 */
@Named("playlists")
@ApplicationScoped
public class YoutubePlaylistPrecacheProvider implements LoadingCacheProvider<YoutubePlaylist> {
    private static final Logger LOGGER = LoggerFactory.getLogger(YoutubePlaylistPrecacheProvider.class);

    @Inject
    YoutubeMediaProviderConfig config;

    @RestClient
    YoutubeAPI api;

    @Override
    public List<YoutubePlaylist> fetchData(ParameterizedCacheKey k) {
        try {
            LOGGER.info("Caching playlists for channel: {}", k.getId());

            // Get first page of results, using max amount
            YoutubePlaylistResponse response = api.getPlaylistsByChannel(YoutubeRequestParams.builder()
                    .setChannelId(k.getId())
                    .setMaxResults(50)
                    .setKey(config.apiKey())
                    .build());

            List<YoutubePlaylist> out = new ArrayList<>(response.getItems());

            // Iterate over every page of YT API results
            while (response.getNextPageToken() != null) {
                response = api.getPlaylistsByChannel(
                        YoutubeRequestParams.builder()
                                .setChannelId(k.getId())
                                .setPageToken(response.getNextPageToken())
                                .setMaxResults(50)
                                .setKey(config.apiKey()).build());

                // Add each item to our final list
                out.addAll(response.getItems());
            }

            // Update all embed URLs to https before adding to cache
            return out.stream().map(this::updateEmbedUrlToHttps).collect(Collectors.toList());
        } catch (Exception e) {
            throw new ProcessingException(String.format("Error while loading cache entry for: %s", k.getId()), e);
        }
    }

    @Override
    public Class<YoutubePlaylist> getType() {
        return YoutubePlaylist.class;
    }

    /**
     * Updates a YoutubePlaylist entity's embed URL from http to https.
     * 
     * @param src The YoutubePlaylist to update
     * @return The updated YoutubePlaylist with https embed URL
     */
    private YoutubePlaylist updateEmbedUrlToHttps(YoutubePlaylist src) {
        String currentEmbed = src.getPlayer().getEmbedHtml();
        String secureEmbed = currentEmbed.replaceFirst("http:", "https:");

        // Update embed URL with https
        return src.toBuilder()
                .setPlayer(src.getPlayer().toBuilder()
                        .setEmbedHtml(secureEmbed)
                        .build())
                .build();
    }
}
