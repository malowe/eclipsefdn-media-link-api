/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.api.models;

import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Response object for queries to the YT-API at '/playlistItems'.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_YoutubePlaylistItemResponse.Builder.class)
public abstract class YoutubePlaylistItemResponse {

    public abstract String getEtag();

    @JsonProperty("pageInfo")
    public abstract PageInfo getPageInfo();

    @Nullable
    @JsonProperty("nextPageToken")
    public abstract String getNextPageToken();

    @Nullable
    @JsonProperty("prevPageToken")
    public abstract String getPrevPageToken();

    public abstract List<PlaylistItem> getItems();

    public static Builder builder() {
        return new AutoValue_YoutubePlaylistItemResponse.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setEtag(String etag);

        @JsonProperty("pageInfo")
        public abstract Builder setPageInfo(PageInfo info);

        @JsonProperty("nextPageToken")
        public abstract Builder setNextPageToken(@Nullable String token);

        @JsonProperty("prevPageToken")
        public abstract Builder setPrevPageToken(@Nullable String token);

        public abstract Builder setItems(List<PlaylistItem> items);

        public abstract YoutubePlaylistItemResponse build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_YoutubePlaylistItemResponse_PlaylistItem.Builder.class)
    public abstract static class PlaylistItem {

        public abstract String getEtag();

        public abstract String getId();

        @Nullable
        public abstract ItemSnippet getSnippet();

        public abstract ItemContentDetails getContentDetails();

        public static Builder builder() {
            return new AutoValue_YoutubePlaylistItemResponse_PlaylistItem.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setEtag(String etag);

            public abstract Builder setId(String id);

            @JsonProperty("snippet")
            public abstract Builder setSnippet(@Nullable ItemSnippet snippet);

            @JsonProperty("contentDetails")
            public abstract Builder setContentDetails(ItemContentDetails details);

            public abstract PlaylistItem build();
        }

    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_YoutubePlaylistItemResponse_ItemSnippet.Builder.class)
    public abstract static class ItemSnippet {

        public abstract String getPlaylistId();

        public static Builder builder() {
            return new AutoValue_YoutubePlaylistItemResponse_ItemSnippet.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            @JsonProperty("playlistId")
            public abstract Builder setPlaylistId(String id);

            public abstract ItemSnippet build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_YoutubePlaylistItemResponse_ItemContentDetails.Builder.class)
    public abstract static class ItemContentDetails {

        public abstract String getVideoId();

        public static Builder builder() {
            return new AutoValue_YoutubePlaylistItemResponse_ItemContentDetails.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            @JsonProperty("videoId")
            public abstract Builder setVideoId(String id);

            public abstract ItemContentDetails build();
        }
    }
}